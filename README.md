LaTeX4LingUSB v.0.1 (14.05.2009) for Windows
================
This is the main page of the Wiki. At the moment I will be focusing on putting together a documentation of how to install and calibrate LaTeX4LingUSB, but as soon as this is done, I will be adding pages with useful links and a collection of some tips on typesetting Linguistics with LaTeX.

Come back for more! 

____

##About LaTeX4LingUSB##

**LaTeX4LingUSB** is a **portable LaTeX installation** for typesetting documents in **Linguistics**. It was compiled as part of a LaTeX workshop delivered at the 14th Essex Graduate Conference in Linguistics (EGCL14) on 06.05.2009.

Based on [Alexander Grahn's (2007) HOWTO](http://www.ctan.org/tex-archive/info/MiKTeX+Ghostscript+GSview+USB-drive-HOWTO.txt) on setting up a portable installation of MiKTeX, GhostScript and GSview, it works as a stand-alone installation on external storage devices such as USB drives, for use with MS Windows computers. To these, we have added an installation of TeXnic Center editor and have pre-installed some of the most commonly used packages for typesetting LaTeX documents in Linguistics.

##How to install LaTeX4LingUSB##

Unzip the LaTeX4LingUSB-v.0.1-2009.05.14.zip file. Copy and paste the files to your USB drive or network drive. This might take some time.
Starting up LaTeX4LingUSB

To start up the portable installation, double-click on the init.bat icon. This will prompt a windows command prompt window. Nothing worrying; but be patient because this might take a while, as the LATEX files are loaded. If all goes well, <init.bat> will prompt with a message informing you that the temporary installation was successful.

Press a key to continue.

##Running TeXnic Center for the first time##

Double-click on the TeXnic Center.exe icon to run the editor. If a wizard window pops-up, accept all default options.

When TeXnic Center asks you to:

enter the full path of the directory where the executables (latex, tex, etc) of your TEX distribution are located.

write the following path in the white box:

D:\texmf\miktex\bin\

where D is your USB drive letter. Then accept all defaults again.

If no window pops up this means that either MiKTEX is already customised for you or that you might need to redefine the Output Profiles. Press Alt + F7; this will open the Output Profiles window. In all three paths, listed, change the USB drive letter to your default letter.

##Halting LaTeX4LingUSB##

Before disconnecting your USB or logging off your network drive, you need to reset the temporary installation of LaTeX4LingUSB. Double click on the reset.bat icon and follow the on screen instructions.

##LaTeX system version information##

MiKTeX 2.7

Ghostscript 8.63

GSview 4.9

TeXNic Center 1.0 stable

##List of LaTeX packages included in LaTeX4LingUSB v.0.1##

    2in1,
    apacite, arabtex, arydshin, avm,
    babel, base, beamer,
    caption, classicthesis, color, colortab, config, covington, currvita
    elsarticle, extsizes
    fancyhdr, footmisc
    gb4e, geometry, graphics,
    harvard, hyperref,
    kluwer,
    latex2html, lingmacros,
    marvosym, multirow,
    natbib,
    palatino, parsetree, pict2e, pifont, pslatex, psnfss, psnfssx, pstricks
    qtree
    rotating
    semantic, setspace, soul, stmaryrd, symbol,
    times, tipa, titlesec, tools, tree-dvips, twoup,
    wrapfig
    xcolor, xyling,
    zapfchan, zapfding 

##Licencing information##

LaTeX4LingUSB is made available under the terms of the GNU GENERAL PUBLIC LICENSE Version 3 and is thus free software. You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License or any later version.

This collection is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

The distributions contained in it are also free software, but might be distributed under different licenses.

<<<<<<< HEAD
A copy of the GNU General Public License is included in this distribution in a .txt file (*gpl-3.0.txt*).
=======
A copy of the GNU General Public License is included in this distribution in a .txt file (*gpl-3.0.txt*). 
>>>>>>> 0f1e73407259af064cc578d8358c18a350a42de0
